# More-Trait-Techs-2
Stellaris Mod More Trait Techs (2)

###  *Supported Version: 2.3.** ###
##  Description: ##
This very simple little Mod, adds 2 new rare Technologies Tier 3 that each add 1 gene Point.
The AI uses this too.

## Features: ##
Adds 2 Rare Technologies that add 1 Gene Point.
The AI uses this too.

## Incompatibilities: ##
Should Work with every other Mod.

---

#### Support: ####
*https://jira.meridanas.me/servicedesk/customer/portal/5*

#### Direct Download: ####
*https://bitbucket.org/meridanas/more-trait-techs-2/downloads/*